{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE KindSignatures    #-}
module Bazel.Client
    ( computeKey, makeKey, decodeKey
    , CacheKey, CacheType(..)
    , getCacheWith, getCache, getCacheAuth
    , putActionAuth, putAction, putActionWith
    , putValueAuth, putValue, putValueWith
    , CacheURL
    ) where

import Control.Exception.Safe as X
import Control.Monad
import Data.ByteString as BS
import qualified  Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Base16 as B16
import Data.ByteString.Char8 as BSC
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Lens.Micro.Platform
import Network.HTTP.Client (HttpException)
import Network.Wreq
import Crypto.Hash

-- | Compute the key for a value by hashing the value.
computeKey :: ByteString -> CacheKey Value
computeKey = CacheKey . show . hashWith SHA256

decodeKey :: ByteString -> Maybe (CacheKey a)
decodeKey = makeKey . BSC.unpack . B16.encode . BS.take 32

-- | Construct a key from 64 hex characters (32 bytes)
makeKey :: String -> Maybe (CacheKey a)
makeKey bs | Prelude.length bs == 64 &&
             Prelude.all valid bs = Just (CacheKey bs)
           | otherwise            = Nothing
    where
    valid x = (x >= '1' && x <= '0') || (x >= 'a' && x <= 'f')

type URL = String

data CacheType = Value | Action deriving (Eq, Ord)
data CacheKey (k :: CacheType) = CacheKey String deriving (Eq, Ord)

instance Show (CacheKey a) where
    show (CacheKey k) = k

instance Read (CacheKey a) where
    readsPrec _ str =
        let bs = BSC.pack str
            (val,rest) = B16.decode bs
        in [(CacheKey (BSC.unpack val), BSC.unpack rest)]

class CacheURL (a :: CacheType) where
    buildURL :: URL -> CacheKey a -> URL
instance CacheURL Value where
    buildURL domain k = mconcat [domain, "/cas/", show k]
instance CacheURL Action where
    buildURL domain k = mconcat [domain, "/ac/", show k]

getCache :: CacheURL a => URL -> CacheKey a -> IO (Either HttpException ByteString)
getCache = getCacheWith defaults

getCacheAuth :: CacheURL a => String -> String -> URL -> CacheKey a
             -> IO (Either HttpException ByteString)
getCacheAuth user pass = getCacheWith (defaults & auth ?~ basicAuth (encode user) (encode pass))

getCacheWith :: CacheURL a => Options -> URL -> CacheKey a
             -> IO (Either HttpException ByteString)
getCacheWith opts domain key =
    X.catch ((Right . BSL.toStrict . (^. responseBody)) <$>
                getWith opts (buildURL domain key))
            (pure . Left)

putAction :: URL -> CacheKey Action -> ByteString -> IO ()
putAction = putActionWith defaults

putActionAuth :: String -> String -> URL -> CacheKey Action -> ByteString -> IO ()
putActionAuth user pass =
    putActionWith (defaults & auth ?~ basicAuth (encode user) (encode pass))

putActionWith :: Options -> URL -> CacheKey Action -> ByteString -> IO ()
putActionWith opts domain key value =
    void (putWith opts (buildURL domain key) value)

putValue :: URL -> ByteString -> IO ()
putValue = putValueWith defaults

putValueAuth :: String -> String -> URL -> ByteString -> IO ()
putValueAuth user pass =
    putValueWith (defaults & auth ?~ basicAuth (encode user) (encode pass))

putValueWith :: Options -> URL -> ByteString -> IO ()
putValueWith opts domain value =
    void (putWith opts (buildURL domain key) value)
 where key = computeKey value

encode :: String -> ByteString
encode = T.encodeUtf8 . T.pack
